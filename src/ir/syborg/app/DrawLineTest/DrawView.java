package ir.syborg.app.DrawLineTest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;


public class DrawView extends View {

    Paint paint = new Paint();
    View  first;
    View  second;


    public DrawView(Context context, View first, View second) {
        super(context);
        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(10.0f);
        paint.setShadowLayer(25.0f, 50.0f, 50.0f, Color.BLACK);
        this.first = first;
        this.second = second;
    }


    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawLine(first.getX(), first.getY(), second.getX(), second.getY(), paint);
    }
}
