package ir.syborg.app.DrawLineTest;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class ActivityDrawLineTest extends Activity {

    DrawView drawView1, drawView2, drawView3;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        TextView txt_one = (TextView) findViewById(R.id.txt_one);
        TextView txt_two = (TextView) findViewById(R.id.txt_two);
        RelativeLayout rtv_main = (RelativeLayout) findViewById(R.id.rtv_main);

        drawView1 = new DrawView(ActivityDrawLineTest.this, txt_one, txt_two);
        drawView1.setBackgroundColor(Color.WHITE);
        rtv_main.addView(drawView1);

    }
}